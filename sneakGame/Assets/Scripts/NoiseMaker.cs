﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMaker : MonoBehaviour
{
    public float currentVolume = 0; //current volume, starts at 0
    public float volumeDecay = 0.015f; //volume decay per frame 

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (currentVolume > 0) //if volume isn't zero, decay 
        {
            currentVolume -= volumeDecay;
        }
	}
}
