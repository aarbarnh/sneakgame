﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BasePawn : MonoBehaviour
{
    public Transform tf; //get pawns transform
    public Vector3 homePos; //where it spawns is its home, to go back to if needed
    public float moveSpeed = 5.0f; //movement speed
    public float turnSpeed = 100.0f; //turning speed
    public Sprite sprite; //image used to represent pawn

    //noise variables
    public NoiseMaker noise;
    public float moveNoise = 5.0f;
    public float turnNoise = 5.0f;

	// Use this for initialization
	public virtual void Start ()
    {
        tf = GetComponent<Transform>(); //get transform
        noise = GetComponent<NoiseMaker>(); //get noisemaker
        sprite = GetComponent<SpriteRenderer>().sprite; //get sprite 
        homePos = tf.position; //set homePos
	}
	
	// Update is called once per frame
    //Define all virtual functions 
	public virtual void Update ()
    {
		
	}

    public virtual void MoveForward()
    {

    }

    public virtual void MoveBackward()
    {

    }

    public virtual void RotateLeft()
    {

    }

    public virtual void RotateRight()
    {

    }

    public virtual void StrafeLeft()
    {

    }

    public virtual void StrafeRight()
    {

    }
}
