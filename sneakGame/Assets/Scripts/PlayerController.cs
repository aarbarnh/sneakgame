﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : BaseController
{

	// Use this for initialization
	public override void Start ()
    {
        base.Start(); //start controller parent 
	}
	
	// Update is called once per frame
	void Update ()
    {
        //getting key input
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            pawn.RotateLeft();
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            pawn.RotateRight();
        }
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            pawn.MoveForward();
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            pawn.MoveBackward();
        }
        if (Input.GetKey(KeyCode.Q))
        {
            pawn.StrafeLeft();
        }
        if (Input.GetKey(KeyCode.E))
        {
            pawn.StrafeRight();
        }
    }

    void OnTriggerEnter2D(Collider2D other) //on trigger enter (egg's) collect egg and incremenet eggs gathered, also set egg inactive
    {
        GameManager.game.eggsGathered++;
        if (GameManager.game.eggsGathered == 1)
        {
            GameManager.game.eggFour.gameObject.SetActive(true); //making UI egg active
        }
        else if (GameManager.game.eggsGathered == 2)
        {
            GameManager.game.eggThree.gameObject.SetActive(true);
        }
        else if (GameManager.game.eggsGathered == 3)
        {
            GameManager.game.eggTwo.gameObject.SetActive(true);
        }
        else if (GameManager.game.eggsGathered == 4)
        {
            GameManager.game.eggOne.gameObject.SetActive(true);
        }
        other.gameObject.SetActive(false);
    }

    void OnCollisionEnter2D(Collision2D other) //if player hits enemy, take life away
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            LoseLife();
        }
    }

    void LoseLife() //losing a life 
    {
        GameManager.game.currentLives--; //decrement
        pawn.tf.position = pawn.homePos; //return to home

        if (GameManager.game.currentLives == 2) 
        {
            GameManager.game.lifeThree.gameObject.SetActive(false); //making UI life inactive
        }
        else if (GameManager.game.currentLives == 1)
        {
            GameManager.game.lifeTwo.gameObject.SetActive(false);
        }
        else if (GameManager.game.currentLives == 0)
        {
            GameManager.game.lifeOne.gameObject.SetActive(false);
        }
    }
}
