﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager game; //create instance of game manager
    public GameObject player; //get player object to pass information to AI
    public List<GameObject> eggs;

    //screen game objects
    public GameObject gameScreen;
    public GameObject startScreen;
    public GameObject winScreen;
    public GameObject loseScreen;

    //player variables
    public int currentLives = 3;
    public int eggsGathered = 0;
    public Image lifeOne, lifeTwo, lifeThree;
    public Image eggOne, eggTwo, eggThree, eggFour;

    //game state
    public enum GameState
    {
        StartMenu,
        Game,
        Win,
        Lose
    }
    public GameState gameState; //variable to hold the state number for enum

	//runs before start, makes sure only one instance and not to destroy on load
	void Awake ()
    {
		if (game == null) //making the game singleton if null, dont destroy on load, if game exists destroy it
        {
            game = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
	}

    void Start()
    {
        SetScreen(0); //start on start menu
    }
	
	// Update is called once per frame
	void Update ()
    {
		if (currentLives == 0) //if player lost, set to lose screen
        {
            SetScreen(3);
        }
        if (eggsGathered == 4) //if player won set to win screen
        {
            SetScreen(2);
        }
        if (Input.GetKey(KeyCode.Escape)) //keep escape for quick quit
        {
            Application.Quit();
        }

	}

    public void SetScreen(int state) //Changing to game states, the screens
    {
        gameState = (GameState)state;
        ActivateScreen();
    }

    public void QuitButton() //function for all quit buttons 
    {
        Application.Quit();
    }

    void ActivateScreen() //activating screens from setscreen
    {
        switch (gameState)
        {
            case GameState.StartMenu: //start menu
                startScreen.SetActive(true);
                winScreen.SetActive(false);
                loseScreen.SetActive(false);
                gameScreen.SetActive(false);
                eggsGathered = 0;
                currentLives = 3;
                break;
            case GameState.Game: //game screen
                startScreen.SetActive(false);
                winScreen.SetActive(false);
                loseScreen.SetActive(false);
                gameScreen.SetActive(true);
                eggsGathered = 0;
                currentLives = 3;
                ResetGame();
                player.transform.position = new Vector3(0, 0, 0);
                break;
            case GameState.Win: //win screen
                startScreen.SetActive(false);
                winScreen.SetActive(true);
                loseScreen.SetActive(false);
                gameScreen.SetActive(false);
                break;
            case GameState.Lose: //lose screen
                startScreen.SetActive(false);
                winScreen.SetActive(false);
                loseScreen.SetActive(true);
                gameScreen.SetActive(false);
                break;
        }
    }

    public void ResetGame() //reset UI and game objects after anytime game screen is reactivated
    {
        eggFour.gameObject.SetActive(false);
        eggThree.gameObject.SetActive(false);
        eggTwo.gameObject.SetActive(false);
        eggOne.gameObject.SetActive(false);
        lifeOne.gameObject.SetActive(true);
        lifeTwo.gameObject.SetActive(true);
        lifeThree.gameObject.SetActive(true);

        for (int i = 0; i < eggs.Count; i++)
        {
            eggs[i].gameObject.SetActive(true);
        }
    }
}
