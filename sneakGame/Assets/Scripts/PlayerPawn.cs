﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : BasePawn
{

	// Use this for initialization
	public override void Start ()
    {
        base.Start(); //start the parent Start (BasePawn)
	}
	
	// Update is called once per frame
	public override void Update ()
    {
        base.Update(); //start the parent Update 
	}

    //override all BasePawn functions 
    public override void MoveForward() //move forward and add noise 
    {
        tf.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        if (noise != null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, moveNoise);
        }
    }

    public override void MoveBackward() //move backward and add noise 
    {
        tf.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        if (noise != null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, moveNoise);
        }
    }

    public override void RotateLeft() //rotate left and add noise 
    {
        tf.Rotate(0,0, turnSpeed * Time.deltaTime);
        if (noise != null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, turnNoise);
        }
    }

    public override void RotateRight() //rotate right and add noise 
    {
        tf.Rotate(0, 0, -(turnSpeed * Time.deltaTime));
        if (noise != null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, turnNoise);
        }
    }

    public override void StrafeLeft() //strafe left and add noise 
    {
        tf.Translate(Vector3.up * moveSpeed * Time.deltaTime);
        if (noise != null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, moveNoise);
        }
    }

    public override void StrafeRight() //strafe right and add noise 
    {
        tf.Translate(Vector3.down * moveSpeed * Time.deltaTime);
        if (noise != null)
        {
            noise.currentVolume = Mathf.Max(noise.currentVolume, moveNoise);
        }
    }

}
