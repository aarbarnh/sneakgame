﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //camera variables 
    private Transform tf;
    private Vector3 offset;
    public Transform playerTf;

	// Use this for initialization
	void Start ()
    {
        tf = GetComponent<Transform>();
        offset = transform.position - playerTf.position; //offset for camera 
	}
	
	// Update is called once per frame
	void LateUpdate () //last of update called 
    {
        tf.position = playerTf.position + offset; //update camera position
	}
}
