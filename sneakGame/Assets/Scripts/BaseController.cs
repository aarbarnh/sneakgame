﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseController : MonoBehaviour //parent/base class of all controllers
{
    public BasePawn pawn; //pawn variable 
	// Use this for initialization
	public virtual void Start ()
    {
        pawn = GetComponent<BasePawn>(); //Get BasePawn script/component
	}
}
